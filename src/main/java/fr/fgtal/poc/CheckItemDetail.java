package fr.fgtal.poc;

import com.google.gson.Gson;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author fgenaudet
 */
@Path("/checkItemDetail")
public class CheckItemDetail {
    private static final Gson gson = new Gson();

    @GET
    @Path("/{id}")
    public String detail(@PathParam("id") Integer id) {
        String result;
        CheckItem item = CheckItemDatabase.getInstance().getCheckItemById(id);
        if (item == null) {
            result = "ERROR";
        } else {
            result = gson.toJson(item);
        }
        return result;
    }
}
