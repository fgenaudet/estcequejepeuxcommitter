/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fgtal.poc;

/**
 *
 * @author fgenaudet
 */
public class CheckItem {

    private Integer id;
    private String description;
    private String message;

    public CheckItem(Integer id, String description, String message) {
        this.id = id;
        this.description = description;
        this.message = message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
