package fr.fgtal.poc;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author fgenaudet
 */
@Path("/checkItemService")
public class CheckItemService {

    private static final Gson gson = new Gson();
    public static final String RELEASE = "release";

    @GET
    @Path("/{module}")
    public String list(@PathParam("module") String module) {
        List<CheckItem> checkItemList = initCheckItemList(module);
        return gson.toJson(checkItemList);
    }

    private List<CheckItem> initCheckItemList(String module) {
        List<CheckItem> resultList;
        if (RELEASE.equals(module)) {
            resultList = buildReleaseModuleCheckItems();
        } else {
            resultList = buildCommitModuleCheckItems();
        }

        return resultList;
    }

    private List<CheckItem> buildCommitModuleCheckItems() {
        return CheckItemDatabase.getInstance().getCheckItemsByIdrange(1, 4);
    }

    private List<CheckItem> buildReleaseModuleCheckItems() {
        return CheckItemDatabase.getInstance().getCheckItemsByIdrange(5, 9);
    }
}
