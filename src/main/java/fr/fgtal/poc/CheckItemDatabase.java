/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fgtal.poc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author fgenaudet
 */
public class CheckItemDatabase {
    
    private static Map<Integer, CheckItem> db = new HashMap<Integer, CheckItem>();
    private static final CheckItemDatabase instance = new CheckItemDatabase();
    
    public static final CheckItemDatabase getInstance() {
        return instance;
    }
    
    public CheckItemDatabase() {
        int itemId = 1;

        // COMMIT [1 -> 4]
        db.put(itemId++, new CheckItem(1, "Avez-vous compil� votre code en local ?", "Non, vous avez envie de casser la compile et d'ammener les croissants ? "));
        db.put(itemId++, new CheckItem(2, "Avez-vous compil� votre code en local AVEC les jsp?", "Hey!! Les Jsps aussi sont compil�es ;) "));
        db.put(itemId++, new CheckItem(3, "Avez-vous lanc� la suite de test JUnit?", "Attention aux effets de bords! Lancer la suite Junit pour en avoir le coeur net."));
        db.put(itemId++, new CheckItem(4, "Avez-vous v�rifi� PMD, CPD, Checkstyle et Findbugs?", "Ca marche peut-�tre mais c'est peut-�tre aussi tr�s sale!!!!"));
        // RELEASE [5 -> 9]
        db.put(itemId++, new CheckItem(1, "Avez-vous v�rifi� les status des JIRAs ?", "//TODO"));
        db.put(itemId++, new CheckItem(2, "Avez-vous v�rifi� les conf. notes des JIRAs ?", "//TODO"));
        db.put(itemId++, new CheckItem(3, "Avez-vous v�rifi� les warning notes des JIRAs ?", "//TODO"));
        db.put(itemId++, new CheckItem(4, "Avez-vous v�rifi� les sql notes des JIRAs ?", "//TODO"));
        db.put(itemId++, new CheckItem(5, "Avez-vous v�rifi� les scripts sql du dossier database ?", "//TODO"));
    }
    
    public CheckItem getCheckItemById(Integer id) {
        return db.get(id);
    }
    
    public List<CheckItem> getCheckItemsByIdrange(Integer startId, Integer stopId) {
        List<CheckItem> checkItemList = new ArrayList<CheckItem>();
        for (Map.Entry<Integer, CheckItem> entryItem : db.entrySet()) {
            if (entryItem.getKey() >= startId && entryItem.getKey() <= stopId) {
                checkItemList.add(entryItem.getValue());
            }
        }
        return checkItemList;
    }
}
