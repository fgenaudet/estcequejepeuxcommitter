$(function() {
    var CheckItem = Backbone.Model.extend();
    var CheckView = Backbone.View.extend({
        tagName: 'li',
        render: function() {
            var html = this.model.get('description') + ' <br/><button value="O" class="yes">Oui</button>'
                    + '&nbsp;<button value="N" class="no">Non</button>';
            this.$el.html(html);
        },
        events: {
            "click button": "displayMessage"
        },
        displayMessage: function(e) {
            var choix = e.srcElement.value;
            var id = +this.model.get('id');
            if ('O' === choix) {
                if (id === checkItemCollection.length) {
                    var date = new Date();
                    if (date.getHours() > 17 || date.getHours() < 6) {
                        $('#canYouCommit').text('A commit ?! At ' + date.toLocaleTimeString()
                                + ' ?? Are you crazy? Do you really want to be at work at 7:45 AM tomorrow ?');
                    } else {
                        $('#canYouCommit').text('Finally, yes you can');
                    }
                } else {
                    $('#canYouCommit').text('');
                }
                showNext(id);
            } else {
                hideNext(id);
                $('#canYouCommit').text(this.model.get('message'));
            }
        }
    });

    var CheckListView = Backbone.View.extend({
        addOne: function(checkItem) {
            var itemId = +checkItem.get('id');
            var checkView = new CheckView({model: checkItem, id: itemId});
            checkView.render();
            this.$el.append(checkView.el);
        },
        render: function() {
            this.collection.forEach(this.addOne, this);
            $('#checkList li[id!="1"]').css('display', 'none');
            checkItemCollection = this.collection;
        },
        initialize: function() {
            var self = this;
            this.collection = new (Backbone.Collection.extend({model: CheckItem, url: this.options.url}));
            this.collection.fetch({
                success: function() {
                    self.render();
                }
            });
        }
    });

    function initList() {
        var url = 'rest/checkItemService/' + $('nav ul li.selected').data('module');
        var checkList = new CheckListView({url: url});
        $('#checkList').html(checkList.el);
        $('#canYouCommit').text('');
    }

    function selectNewNav() {
        $('nav ul li').removeClass("selected");
        $('header h1').text($(this).text());
        $(this).addClass("selected");
        initList();
    }



    function showNext(id) {
        $('#checkList li[id="' + (id + 1) + '"]').show('fast');
    }
    function hideNext(id) {
        $('#checkList li[id!="' + id + '"]').each(
                function(index, el) {
                    if (el.id >= id) {
                        $(el).hide('fast');
                    }
                }
        );
    }

    // Behaviours 
    $('nav').on('click', 'ul li', selectNewNav);

    // At launch
    $('#checkList').text("Choisissez un module.");
});
