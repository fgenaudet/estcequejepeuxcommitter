/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fgtal.poc;

/**
 *
 * @author fgenaudet
 */
import org.junit.rules.ExternalResource;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.webapp.WebAppContext;

public class EmbeddedTestServer extends ExternalResource {
	Server server;
	int port = 9000;

	@Override
	protected void before() throws Throwable {
		server = new Server(port);
		server.addHandler(new WebAppContext("src/main/webapp", "/"));
		server.start();
	}

	@Override
	protected void after() {
		try {
			server.stop();
		} catch (Throwable t) {
                    //Logger log4j ??
                    t.printStackTrace();
                }
	}

	public String uri() {
		return "http://localhost:" + port;
	}
}