/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.fgtal.poc;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import java.net.URI;
import java.util.List;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriBuilderException;
import org.apache.commons.lang3.StringUtils;
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Rule;

public class CheckItemServiceTest {

    public static final String COMMIT_MODULE = "commit";
    public static final String RELEASE_MODULE = "release";
    @Rule
    public EmbeddedTestServer server = new EmbeddedTestServer();

    @Test
    public void testGetCommitCheckItems() {
        List<CheckItem> checkItemList = getCheckItems(COMMIT_MODULE);

        assertNotNull("Commit check item list should not be null", checkItemList);
        assertEquals("Commit check item list should have a size of ", 4, checkItemList.size());
    }

    @Test
    public void testGetCommitCheckItemDetail() {
        CheckItem checkItem = getCheckItem(1);

        assertTrue("checkItem should be a CheckItem", checkItem instanceof CheckItem);
        assertNotNull("checkItem id should not be null", checkItem.getId());
        assertEquals("checkItem id should be 1", "1", checkItem.getId().toString());

        assertNotNull("checkItem description should not be null", checkItem.getDescription());
        assertTrue("checkItem description should not be empty", StringUtils.isNotEmpty(checkItem.getDescription()));
        
        assertNotNull("checkItem message should not be null", checkItem.getMessage());
        assertTrue("checkItem message should not be empty", StringUtils.isNotEmpty(checkItem.getMessage()));
    }

    @Test
    public void testGetReleaseCheckItems() {
        List<CheckItem> checkItemList = getCheckItems(RELEASE_MODULE);

        assertNotNull("Release check item list should not be null", checkItemList);
        assertEquals("Release check item list should have a size of ", 5, checkItemList.size());
    }

    private List<CheckItem> getCheckItems(String module) throws UriBuilderException, IllegalArgumentException {
        URI uri = UriBuilder.fromPath("rest/checkItemService/{module}").build(module);
        WebResource resource = Client.create().resource(server.uri()).path(uri.getPath());
        final String checkItemJson = resource.get(String.class);
        return new Gson().fromJson(checkItemJson, List.class);
    }

    private CheckItem getCheckItem(/*String module, */long id) throws UriBuilderException, IllegalArgumentException {
        URI uri = UriBuilder.fromPath("rest/checkItemDetail/{id}").build(id);
        WebResource resource = Client.create().resource(server.uri()).path(uri.getPath());
        final String checkItemJson = resource.get(String.class);
        return new Gson().fromJson(checkItemJson, CheckItem.class);
    }
}